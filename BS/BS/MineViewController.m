//
//  MineViewController.m
//  BS
//
//  Created by Jiaqi Chen on 4/3/14.
//  Copyright (c) 2014 Jiaqi Chen. All rights reserved.
//

#import "MineViewController.h"
#import "SWRevealViewController.h"
#import <Parse/Parse.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKitDefines.h>

@interface MineViewController (){
    NSString *like_UDID;
    NSString *macaddress;
    
    PFObject *MarkedBook;
    
    PFQuery *markedBook;
    PFObject *OfferedBook;
    PFFile *imageFile;
    //PFQueryTableViewController *child;
}

@end

@implementation MineViewController

@synthesize SideBarMenuButton,Marked,TableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (NSString *) identifierForVendor1
{
    if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
        return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    }
    return @"";
}


- (void) retrievingDataFromParse{
    PFQuery *query = [PFQuery queryWithClassName:@"MarkedBook"];

    [query whereKey:@"macaddress" equalTo:macaddress];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %d data.", objects.count);
            
            for (PFObject *object in objects) {
                
                NSLog(@"%@",object.objectId);
                [Marked addObject:object];
                NSLog(@"%@",Marked);
                NSLog(@"%@",[Marked objectAtIndex:0]);
                NSLog(@"%@",[[Marked objectAtIndex:0] objectForKey:@"title"]);
                
            }
            
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
        
        
    }];
}

- (void)viewDidLoad
{

    SideBarMenuButton.target = self.revealViewController;
    SideBarMenuButton.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    like_UDID=[NSString stringWithFormat:@"%@",[[UIDevice currentDevice] identifierForVendor]];
    macaddress = [like_UDID substringFromIndex:29];
    NSLog(@"%@",macaddress);
    self.Marked = [NSMutableArray array];
    [self retrievingDataFromParse];
    
    self.segmentcontrol.selectedSegmentIndex = 0;
    [super viewDidLoad];
    
}


- (void)viewWillAppear:(BOOL)animated{

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"%lu",(unsigned long)Marked.count);
    return Marked.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MarkedBookCell" forIndexPath:indexPath];
    

    if(self.segmentcontrol.selectedSegmentIndex == 0)
    {
        
        cell.textLabel.text = [[Marked objectAtIndex:indexPath.row] objectForKey:@"title"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Provided by: %@",[[Marked objectAtIndex:indexPath.row] objectForKey:@"provider"]];
    
        UIImageView *CellImageView = (UIImageView*)[[cell contentView] viewWithTag:1];
        
        PFFile *imageFile;
        imageFile = [[Marked objectAtIndex:indexPath.row] objectForKey:@"bookcover"];
        //image = [UIImage imagewith];
        [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                UIImage *image = [UIImage imageWithData:data];
                // image can now be set on a UIImageView
                CGSize size = CGSizeMake(80, 60);
                UIImage *resizedimage = [self resizeImage:image imageSize:size];
                CellImageView.image = resizedimage;
            }
        }];
        //[self.TableView reloadData];
        
    }
    
    else if (self.segmentcontrol.selectedSegmentIndex == 1)
    {
        cell.textLabel.text = @"Introduction to Algorithm";
        cell.detailTextLabel.text = @"provided by Yixuan Zhao";
        UIImageView *CellImageView = (UIImageView*)[[cell contentView] viewWithTag:1];
        UIImage *image = [UIImage imageNamed:@"Introduction_to_Algorithm.jpeg"];
        CGSize size = CGSizeMake(80, 60);
        UIImage *resizedimage = [self resizeImage:image imageSize:size];
        CellImageView.image = resizedimage;
    }
    
    return cell;
}

//resize the image
-(UIImage*)resizeImage:(UIImage *)image imageSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0,0,size.width,size.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    //here is the scaled image which has been changed to the size specified
    UIGraphicsEndImageContext();
    return newImage;
    
}


- (IBAction)segmentedControlChanged:(id)sender {
    [self.TableView reloadData];
}
@end
