//
//  DetailViewController.h
//  BS
//
//  Created by Jiaqi Chen on 4/3/14.
//  Copyright (c) 2014 Jiaqi Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiscoverBookViewController.h"

@interface BookInfoViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (strong) NSString *Macaddress;
@property (weak, nonatomic) IBOutlet UILabel *booktitle;
@property (weak, nonatomic) IBOutlet UILabel *author;
@property (weak, nonatomic) IBOutlet UILabel *location;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *avalabletime;
@property (weak, nonatomic) IBOutlet UIImageView *bookcover;
@property (weak, nonatomic) IBOutlet UITextView *contact;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
