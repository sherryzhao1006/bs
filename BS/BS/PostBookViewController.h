//
//  PostBookViewController.h
//  BS
//
//  Created by Jiaqi Chen on 4/3/14.
//  Copyright (c) 2014 Jiaqi Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostBookViewController : UIViewController<UITextFieldDelegate,UITextInputDelegate>
@property (weak, nonatomic) IBOutlet UITextView *PostTextView;
@property (weak, nonatomic) IBOutlet UITextField *ISBNField;
@property (weak, nonatomic) IBOutlet UITextField *LocationField;
@property (weak, nonatomic) IBOutlet UITextField *PriceField;
@property (weak, nonatomic) IBOutlet UITextField *AvalableTimeField;
@property (weak, nonatomic) IBOutlet UITextView *providerField;

@property (weak, nonatomic) IBOutlet UIImageView *BookCover;


@end
