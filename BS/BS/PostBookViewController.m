//
//  PostBookViewController.m
//  BS
//
//  Created by Jiaqi Chen on 4/3/14.
//  Copyright (c) 2014 Jiaqi Chen. All rights reserved.
//

#import "PostBookViewController.h"
#import <Parse/Parse.h>
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>

@interface PostBookViewController () <UITextViewDelegate>

@end

@implementation PostBookViewController
{
    PFObject *PublicBook;
    PFFile *imageFile;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.toolbarHidden = NO;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard{
    [self.ISBNField resignFirstResponder];
    [self.PriceField resignFirstResponder];
    
    [self.LocationField resignFirstResponder];
    [self.AvalableTimeField resignFirstResponder];
    [self.providerField resignFirstResponder];

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    NSLog(@"Return my key was hit! which =%@",textField.text);
    return YES;
}

-(void) animateTextView:(UITextView *)textView up:(BOOL)up{
    const int movedis=210;
    const float movedur=0.3f;
    int movement=(up ?-movedis:movedis);
    [UIView beginAnimations:@"anim" context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:movedur];
    self.view.frame=CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


-(void)textViewDidBeginEditing:(UITextView *)textView{
    NSLog(@"a");
    [self animateTextView:textView up:YES];
    //[textView resignFirstResponder];
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"b");
    [self animateTextView:textView up:NO];
    [textView resignFirstResponder];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    UIImage *image;
    image = [UIImage imageNamed:@"science_of_programming.png"];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.05f);
    
    imageFile = [PFFile fileWithName:@"science_of_programming.png" data:imageData];
    

    
    if ([[segue identifier] isEqualToString:@"save"]) {
        
        PublicBook = [PFObject objectWithClassName:@"PublicBook"];
        
        
        PublicBook[@"Location"] = self.LocationField.text;
        
        PublicBook[@"price"] = self.PriceField.text;
        
        PublicBook[@"ISBN"] = self.ISBNField.text;
        
        PublicBook[@"avaliabletime"] = self.AvalableTimeField.text;
        
        PublicBook[@"Contact"] = self.providerField.text;
        
        PublicBook[@"bookcover"] = imageFile;
        
        
        [PublicBook saveInBackground];

}
    
    }
@end