//
//  SearchLocationViewController.h
//  BS
//
//  Created by Jiaqi Chen on 4/3/14.
//  Copyright (c) 2014 Jiaqi Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchLocationViewController : UITableViewController<UISearchDisplayDelegate,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) id detailItem;
@property (nonatomic,strong)NSMutableArray *dataSource;
@property (nonatomic,strong)NSMutableArray *dataBase;
@property (nonatomic,strong)UISearchDisplayController *mySearchDisplayController;
@end
