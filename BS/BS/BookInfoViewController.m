//
//  DetailViewController.m
//  BS
//
//  Created by Jiaqi Chen on 4/3/14.
//  Copyright (c) 2014 Jiaqi Chen. All rights reserved.
//

#import "BookInfoViewController.h"
#import "MineViewController.h"

@interface BookInfoViewController ()
- (void)configureView;
@end

@implementation BookInfoViewController{
    PFObject *MarkedBook;
    PFFile *imageFile;
}

@synthesize Macaddress;

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationController.toolbarHidden = NO;
    
    self.booktitle.text = [self.detailItem objectForKey:@"title"];
    self.author.text = [self.detailItem objectForKey:@"author"];
    self.price.text = [self.detailItem objectForKey:@"price"];
    self.avalabletime.text = [self.detailItem objectForKey:@"avalabletime"];
    self.location.text = [self.detailItem objectForKey:@"location"];
    self.contact.text = [self.detailItem objectForKey:@"contact"];
    
    PFFile *imageFile;
    imageFile = [self.detailItem objectForKey:@"bookcover"];
    //image = [UIImage imagewith];
    [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!error) {
            UIImage *image = [UIImage imageWithData:data];
            // image can now be set on a UIImageView
            self.bookcover.image = image;
        }
    }];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"mark"]) {
        
        MarkedBook = [PFObject objectWithClassName:@"MarkedBook"];
        
        MarkedBook[@"title"] = [self.detailItem objectForKey:@"title"];
        MarkedBook[@"provider"] = [self.detailItem objectForKey:@"provider"];
        MarkedBook[@"price"] = [self.detailItem objectForKey:@"price"];
        MarkedBook[@"macaddress"] = self.Macaddress;
        
        MarkedBook[@"bookcover"] = [self.detailItem objectForKey:@"bookcover"];
        
        MarkedBook[@"contact"] = [self.detailItem objectForKey:@"contact"];
        
        [MarkedBook saveInBackground];

        
    }
}

@end
