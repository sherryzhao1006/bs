//
//  SearchBookViewController.m
//  BS
//
//  Created by Jiaqi Chen on 4/3/14.
//  Copyright (c) 2014 Jiaqi Chen. All rights reserved.
//

#import "SearchBookViewController.h"
#import "BookInfoViewController.h"

@interface SearchBookViewController ()

@end

@implementation SearchBookViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _dataSource=[[NSMutableArray alloc]init];
    _dataBase=[[NSMutableArray alloc]init];
    for (int i=0; i<50; i++) {
        [_dataSource  addObject:[NSString stringWithFormat:@"%d",i]];
        [_dataBase addObject:[NSString stringWithFormat:@"%d",i]];
    }
    UISearchBar *searchBar=[[UISearchBar alloc]init];
    searchBar.frame=CGRectMake(0, 0, self.view.bounds.size.width+20, 0);
    searchBar.delegate=self;
    searchBar.keyboardType=UIKeyboardTypeDefault;
    searchBar.showsCancelButton=YES;
    searchBar.showsSearchResultsButton=YES;
    searchBar.translucent=YES;
    searchBar.barStyle=UIBarStyleDefault;
    searchBar.placeholder=@"input";
    //searchBar.prompt=@"search";
    [searchBar sizeToFit];
    self.tableView.tableHeaderView=searchBar;
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_dataSource count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str=@"Cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:str forIndexPath:indexPath];
    cell.textLabel.text=[_dataSource objectAtIndex:indexPath.row];
    if (_dataSource.count==0) {
        cell.textLabel.text=@"Sorry, we didn't see and matched book";
    }
    return cell;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [_dataSource removeAllObjects];
    NSString *data;
    for(data in _dataBase){
        if ([data hasPrefix:searchBar.text]) {
            [_dataSource addObject:data];
        }
    }
    
    [self.tableView reloadData];
    [searchBar resignFirstResponder];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.text=@" ";
    [searchBar resignFirstResponder];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (0==searchText.length) {
        return;
    }
    [_dataSource removeAllObjects];
    NSString *str;
    UITableViewCell *cell=(UITableViewCell *)[self.view viewWithTag:1];
    for(str in _dataBase){
        if ([str hasPrefix:searchText]) {
            [_dataSource addObject:str];
        }
        else
        {
            //cell.textLabel.text=@"Sorry, we didn't see and matched book";
        }
        
    }
    if (_dataSource.count==0) {
        cell.textLabel.text=@"Sorry, we didn't see and matched book";
    }
    [self.tableView reloadData];
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
